import { LightningElement, track, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { refreshApex } from "@salesforce/apex";
import findMovies from "@salesforce/apex/MoviesController.findMovies";
import moviesPic from "@salesforce/contentAssetUrl/movies_logo";
import deleteMovie from "@salesforce/apex/MoviesController.deleteMovie";

export default class MoviesManager extends LightningElement {
  @wire(findMovies, { searchKey: "$searchKey" }) movies = [];
  @track searchKey = "";
  @track showCreateModal = false;
  @track showPreview = false;
  @track isUpdateMode = false;
  @track previewedOrUpdateMovie = null;

  /* Creation result messages */
  MOVIES_LIST_ERROR = "Error happened while searching for movies";
  SUCCESS_MESSAGE = "movie created succefully";
  ERROR_MESSAGE = "movie was not created";
  CREATION_TOAST = "Movie Creation";

  /* Movie Static Image */
  MOVIE_PIC = moviesPic;

  /**
   * handle search input changes and reload the movies list
   * @param {*} event the incomming event with the search value
   */
  handleSeach(event) {
    this.searchKey = event.detail;
  }

  /**
   * handle a new movie creation with success
   */
  handleSuccessCreation() {
    const successEvent = new ShowToastEvent({
      title: this.CREATION_TOAST,
      message: this.SUCCESS_MESSAGE,
      variant: "success"
    });
    this.dispatchEvent(successEvent);
    this.showCreateModal = false;
    refreshApex(this.movies);
  }

  /**
   * handle a new movie creation with error
   */
  handleFailCreation() {
    const failEvent = new ShowToastEvent({
      title: this.CREATION_TOAST,
      message: this.ERROR_MESSAGE,
      variant: "error"
    });
    this.dispatchEvent(failEvent);
    this.showCreateModal = false;
  }

  /**
   * handle preview a movie
   * @param {*} event event containing the id of the clicked movie
   */
  handlePreviewMovie(event) {
    const movieId = event.detail;
    this.previewedOrUpdateMovie = this.movies.data.find(
      (movie) => movie.id === movieId
    );
    this.showPreview = true;
  }

  /**
   * handle deleting of a movie
   * @param {*} event event with id of the movie
   */
  handleDeleteMovie(event) {
    const movieId = event.detail;
    deleteMovie({ movieId: movieId }).then(() => {
      this.showPreview = false;
      refreshApex(this.movies);
    });
  }

  handleUpdateMovie(event) {
    const movieId = event.detail;
    this.previewedOrUpdateMovie = this.movies.data.find(
      (movie) => movie.id === movieId
    );
    this.isUpdateMode = true;
    this.showCreateModal = true;
    this.showPreview = false;
  }

  /**
   * hide the preview modal
   */
  // eslint-disable-next-line no-unused-vars
  hidePreview(event) {
    this.showPreview = false;
    this.previewedOrUpdateMovie = null;
  }

  /**
   * show or hide the creation modal
   */
  toggleCreateForm() {
    this.showCreateModal = !this.showCreateModal;
    this.previewedOrUpdateMovie = null;
  }
}
