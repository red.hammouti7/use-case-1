import { api, LightningElement } from "lwc";

export default class MoviesResultsLwc extends LightningElement {
  @api movie;
  @api movieAvatar;

  // eslint-disable-next-line no-unused-vars
  handlePreviewMovie(event) {
    const previewEvent = new CustomEvent("preview", {
      detail: this.movie.id
    });
    this.dispatchEvent(previewEvent);
  }
}
