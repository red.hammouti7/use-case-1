public with sharing class MoviesController {
  /**
   *
   * @param searchKey key to search for movies by names
   * @return  `List<MovieModel>` list of found movies
   * @exception
   */
  @AuraEnabled(cacheable=true)
  public static List<MovieModel> findMovies(String searchKey) {
    try {
      String moviesQuery;
      if (!String.isBlank(searchKey)) {
        String key = '%' + searchKey + '%';
        moviesQuery = 'SELECT id, name, category__c, Release_date__c, description__c FROM Movie__c WHERE Name LIKE :key';
      } else {
        moviesQuery = 'SELECT id, name, category__c, Release_date__c, description__c FROM Movie__c';
      }
      List<SObject> movies = Database.query(moviesQuery);
      return moviesMapper(movies);
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * get the movie categorie picklist values
   * @return  `Map<String, String>` map with label to value picklist values
   */
  @AuraEnabled(cacheable=true)
  public static Map<String, String> getMovieCategories() {
    // Get all picklist values
    List<Schema.PicklistEntry> entries = Movie__C.Category__c.getDescribe()
      .getPickListValues();
    // Only return active picklist values
    Map<String, String> activeCategories = new Map<String, String>();
    for (Schema.PicklistEntry entry : entries) {
      if (entry.isActive()) {
        activeCategories.put(entry.getLabel(), entry.getValue());
      }
    }
    return activeCategories;
  }

  /**
   *
   * @param serializedMovie movie to be added in string format
   * @param actorsIds ids of actors in the movie
   * @return  `String` id of the created movie
   * @exception
   */
  @AuraEnabled
  public static String addMovie(
    String serializedMovie,
    List<String> actorsIds
  ) {
    try {
      MovieModel movie = (MovieModel) JSON.deserialize(
        serializedMovie,
        MovieModel.class
      );
      SObject sMovie = (SObject) Type.forName('Movie__c').newInstance();
      sMovie.put('name', movie.name);
      sMovie.put('Category__c', movie.category);
      sMovie.put('Description__c', movie.description);
      sMovie.put('Release_date__c', Date.valueOf(movie.releaseDate));
      insert sMovie;

      List<SObject> movieActors = new List<SObject>();
      for (String actorId : actorsIds) {
        SObject movieActor = (SObject) Type.forName('MovieActor__c')
          .newInstance();
        movieActor.put('movie__c', sMovie.id);
        movieActor.put('actor__c', actorId);
        movieActors.add(movieActor);
      }
      insert movieActors;

      return String.valueOf(sMovie.Id);
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   *
   * @param serializedMovie movie with new values in string format
   * @param actorsIds list of ids of new actors after update
   * @return  `string` id of the updated movie
   * @exception
   */
  @AuraEnabled
  public static string updateMovie(
    String serializedMovie,
    List<String> actorsIds
  ) {
    try {
      MovieModel movie = (MovieModel) JSON.deserialize(
        serializedMovie,
        MovieModel.class
      );
      // update movie data
      Id movieId = ID.valueOf(movie.id);
      SObject Smovie = Database.query(
        'SELECT Id, Name, Category__c, Description__c FROM Movie__c WHERE id =:movieId LIMIT 1'
      );
      Smovie.put('name', movie.name);
      Smovie.put('Category__c', movie.category);
      Smovie.put('Description__c', movie.description);
      update Smovie;

      // Delete relationship between Movie and Old Actors
      List<SObject> oldMovieActors = Database.query(
        'SELECT Id from MovieActor__c WHERE Movie__c =:movieId'
      );
      delete oldMovieActors;

      // insert new actors
      List<SObject> movieActors = new List<SObject>();
      for (String actorId : actorsIds) {
        SObject movieActor = (SObject) Type.forName('MovieActor__c')
          .newInstance();
        movieActor.put('movie__c', Smovie.id);
        movieActor.put('actor__c', actorId);
        movieActors.add(movieActor);
      }
      insert movieActors;

      return String.valueOf(movie.Id);
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   *
   * @param movieId movie id to be deleted
   * @exception
   */
  @AuraEnabled
  public static void deleteMovie(String movieId) {
    try {
      SObject movie = (SObject) Type.forName('Movie__c').newInstance();
      movie.put('Id', movieId);
      delete movie;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * wrap movies__c records in MovieModel
   * @param objs list of salesforce objects to be transformed
   * @return  `List<MovieModel>` list of transformed movies
   */
  private static List<MovieModel> moviesMapper(List<SObject> objs) {
    List<MovieModel> movies = new List<MovieModel>();
    for (SObject obj : objs) {
      MovieModel movieModel = new movieModel();
      movieModel.name = String.valueOf(obj.get('Name'));
      movieModel.category = String.valueOf(obj.get('Category__c'));
      movieModel.releaseDate = Date.valueOf(obj.get('Release_date__c'));
      movieModel.description = String.valueOf(obj.get('Description__c'));
      movieModel.id = String.valueOf(obj.get('id'));
      movies.add(movieModel);
    }
    return movies;
  }

  // Data Model
  class MovieModel {
    @AuraEnabled
    public String id;
    @AuraEnabled
    public String name;
    @AuraEnabled
    public String category;
    @AuraEnabled
    public String description;
    @AuraEnabled
    public String isReleased;
    @AuraEnabled
    public String femaleActorsPercentage;
    @AuraEnabled
    public String maleActorsPercentage;
    @AuraEnabled
    public Date releaseDate;
  }

  // Data Model
  class MovieActorModel {
    @AuraEnabled
    public String id;
    @AuraEnabled
    public String name;
    @AuraEnabled
    public String actorId;
    @AuraEnabled
    public String movieId;
  }
}
