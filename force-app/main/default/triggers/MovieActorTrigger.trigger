trigger MovieActorTrigger on MovieActor__c(after insert) {
  List<Id> actorsId = new List<ID>();
  for (MovieActor__c movieActor : Trigger.new) {
    actorsId.add(movieActor.Actor__c);
  }
  List<Actor__c> actors = [
    SELECT id, Number_of_movies__c
    FROM Actor__c
    WHERE id IN :actorsId
  ];
  for (Actor__c actor : actors) {
    if (actor.Number_of_movies__c != null) {
      actor.Number_of_movies__c += 1;
    } else {
      actor.Number_of_movies__c = 1;
    }
  }

  update actors;
}
