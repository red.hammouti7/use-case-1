import { LightningElement } from "lwc";

export default class FilterMoviesLwc extends LightningElement {
  /**
   * handle input change
   * @param {*} event event search key value
   */
  handlSearchChange(event) {
    const inputEvent = new CustomEvent("search", {
      detail: event.target.value
    });
    this.dispatchEvent(inputEvent);
  }
}
