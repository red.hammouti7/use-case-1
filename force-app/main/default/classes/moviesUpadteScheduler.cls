global with sharing class moviesUpadteScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    MoviesUpdateBatch mub = new MoviesUpdateBatch();
    Database.executeBatch(mub);
  }
}
