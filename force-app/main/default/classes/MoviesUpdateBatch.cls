global with sharing class MoviesUpdateBatch implements Database.Batchable<SObject> {
  global Database.QueryLocator start(Database.BatchableContext bc) {
    // collect the batches of records or objects to be passed to execute
    String query = 'SELECT Id, Name, Release_date__c  FROM Movie__c';
    return Database.getQueryLocator(query);
  }
  global void execute(Database.BatchableContext bc, List<SObject> movies) {
    for (SObject movie : movies) {
      Date releaseDate = Date.valueOf(movie.get('Release_date__c'));

      if (releaseDate < System.today()) {
        movie.put('IsReleased__c', true);
      }
      update movie;
    }
  }
  global void finish(Database.BatchableContext bc) {
    // execute any post-processing operations
  }
}
