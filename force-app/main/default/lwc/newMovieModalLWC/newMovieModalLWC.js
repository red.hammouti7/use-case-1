/* eslint-disable no-unused-vars */
import { LightningElement, api, wire, track } from "lwc";
import getMovieCategories from "@salesforce/apex/MoviesController.getMovieCategories";
import addMovie from "@salesforce/apex/MoviesController.addMovie";
import updateMovie from "@salesforce/apex/MoviesController.updateMovie";
import getAllActors from "@salesforce/apex/ActorsController.getAllActors";
import getMovieActors from "@salesforce/apex/ActorsController.getMovieActors";
export default class NewMovieModal extends LightningElement {
  @api movieAvatar;
  @track categoriesOptions = [];
  @track actorsOptions = [];
  @track selectedActors = [];
  @track allActors = [];
  @track selectedActorId = null;
  @api isUpdateMode = false;
  @api movie;

  @track
  newMovie = {
    id: "",
    name: null,
    category: null,
    description: "",
    releaseDate: null
  };

  connectedCallback() {
    if (this.isUpdateMode && this.movie) {
      this.newMovie = JSON.parse(JSON.stringify(this.movie));

      getMovieActors({ movieId: this.newMovie.id }).then((data) => {
        // add to selcteActors
        this.selectedActors = data.map((act) => {
          return { label: act.name, value: act.id };
        });
        // remove from actorsOptions
        this.selectedActors.forEach((actor) => {
          const actIndex = this.actorsOptions.findIndex(
            (act) => act.value === actor.value
          );
          this.actorsOptions.splice(actIndex, 1);
        });
      });
    }
  }

  @wire(getMovieCategories)
  getcategories({ data, error }) {
    if (data) {
      this.categoriesOptions = [];
      // eslint-disable-next-line guard-for-in
      for (let key in data) {
        this.categoriesOptions.push({ label: data[key], value: key });
      }
    } else if (error) {
      throw error;
    }
  }

  @wire(getAllActors)
  getActors({ data, error }) {
    if (data) {
      this.allActors = data.map((act) => {
        return { label: act.name, value: act.id };
      });
      // init the actors option list
      this.actorsOptions = [...this.allActors];
    } else if (error) {
      throw error;
    }
  }

  /**
   * handle input fields values and populate the corresponding movies properties
   * @param {*} event event with input value
   */
  handMovieInfochange(event) {
    const eventValue = event.target.value;
    const sourceInputName = event.target.name;
    if (sourceInputName === "movieName") {
      this.newMovie.name = eventValue;
    } else if (sourceInputName === "movieCategory") {
      this.newMovie.category = eventValue;
    } else if (sourceInputName === "movieDescription") {
      this.newMovie.description = eventValue;
    } else if (sourceInputName === "movieRealseDate") {
      this.newMovie.releaseDate = eventValue;
    }
  }

  /**
   * handle selection of an actor
   * @param {*} event
   */
  handleActorSelection(event) {
    const actorInput = this.template.querySelector(".actor-select");
    if (actorInput && actorInput.value) {
      const selectedActore = this.allActors.find(
        (act) => act.value === actorInput.value
      );
      this.selectedActors.push(selectedActore);
      // remove the selected actor from the available actors list (actor can not be choosen twice)
      const optionIndex = this.actorsOptions.findIndex(
        (act) => act.value === actorInput.value
      );
      this.actorsOptions.splice(optionIndex, 1);
    }
  }

  /**
   * handle unselection of an actor
   * @param {*} event
   */
  handleDeselectActor(event) {
    const deselectedActorId = event.target.dataset.id;
    const actorIndex = this.selectedActors.findIndex(
      (act) => act.value === deselectedActorId
    );
    this.selectedActors.splice(actorIndex, 1);
    // put actor back in the available actors list
    const selectedActore = this.allActors.find(
      (act) => act.value === deselectedActorId
    );
    this.actorsOptions.push(selectedActore);
    this.selectedActorId = null;
  }

  /**
   * create the new movie and attach the selected actors to it
   */
  handlesave() {
    const selectedActorsIds = this.selectedActors.map((actor) => actor.value);
    const params = {
      serializedMovie: JSON.stringify(this.newMovie),
      actorsIds: selectedActorsIds
    };
    const methodToUse = this.isUpdateMode
      ? updateMovie(params)
      : addMovie(params);

    methodToUse
      .then((createdMovieId) => {
        this.selectedActors = [];
        const inputEvent = new CustomEvent("success");
        this.dispatchEvent(inputEvent);
      })
      .catch((error) => {
        const inputEvent = new CustomEvent("fail");
        this.dispatchEvent(inputEvent);
      });
  }

  /**
   * cancel creation
   */
  cancelCreation() {
    const inputEvent = new CustomEvent("cancel");
    this.dispatchEvent(inputEvent);
  }

  /**
   * return false if a required field is messing
   */
  get cannotCreate() {
    return !this.newMovie.name || !this.newMovie.category;
  }

  get availableActors() {
    return this.actorsOptions.map((proxyObj) =>
      JSON.parse(JSON.stringify(proxyObj))
    );
  }

  get selectedActorsList() {
    return this.selectedActors.map((actorProxy) =>
      JSON.parse(JSON.stringify(actorProxy))
    );
  }
}
