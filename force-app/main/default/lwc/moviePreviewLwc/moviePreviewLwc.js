import { LightningElement, api } from "lwc";

export default class MoviePreviewLwc extends LightningElement {
  @api movieAvatar;
  @api movie;

  /**
   * emit a cancel event to Movies Manager
   * @param {*} event
   */
  // eslint-disable-next-line no-unused-vars
  handleCancelPreview(event) {
    const cancelEvent = new CustomEvent("cancel");
    this.dispatchEvent(cancelEvent);
  }

  handleDelete() {
    const deleteEvent = new CustomEvent("delete", { detail: this.movie.id });
    this.dispatchEvent(deleteEvent);
  }

  handleUpdate() {
    const updateEvent = new CustomEvent("update", { detail: this.movie.id });
    this.dispatchEvent(updateEvent);
  }
}
