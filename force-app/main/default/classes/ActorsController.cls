public with sharing class ActorsController {
  /**
   * get actors list
   * @return  `List<ActorModel>`
   * @exception
   */
  @AuraEnabled(cacheable=true)
  public static List<ActorModel> getAllActors() {
    try {
      List<SObject> sActors = Database.query('SELECT id, name FROM Actor__c');
      return actorMapper(sActors);
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * list of actors in a specific movie
   * @param movieId id of the movie of the actors list
   * @return  `List<ActorModel>` list of actors in the movie with the movieId
   * @exception
   */
  @AuraEnabled(cacheable=false)
  public static List<ActorModel> getMovieActors(String movieId) {
    try {
      List<SObject> actors = Database.query(
        'SELECT Name, Id FROM Actor__C WHERE Id IN (SELECT Actor__C FROM MovieActor__C WHERE Movie__c = :movieId)'
      );
      //
      return actorMapper(actors);
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * map actors from salesforce objects to ActorModel objects
   * @param dbObjects
   * @return  `List<ActorModel>`
   */
  private static List<ActorModel> actorMapper(List<SObject> dbObjects) {
    List<ActorModel> actors = new List<ActorModel>();
    for (SObject sActor : dbObjects) {
      ActorModel actor = new ActorModel();
      actor.id = String.valueOf(sActor.get('id'));
      actor.name = String.valueOf(sActor.get('Name'));
      actors.add(actor);
    }
    return actors;
  }

  // Data Model
  class ActorModel {
    @AuraEnabled
    public String id;
    @AuraEnabled
    public String name;
  }
}
