trigger MovieActorTriggerUpdatePercentOfActorGenders on MovieActor__c(
  after insert
) {
  // get created movieId
  List<Id> movieId = new List<Id>();
  for (MovieActor__c movieActor : Trigger.new) {
    movieId.add(movieActor.Movie__c);
  }
  List<Movie__c> movieToUpdate = [
    SELECT id, FemaleActorsPercentage__c, MaleActorsPercentage__c
    FROM Movie__c
    WHERE id IN :movieId
  ];

  // get related actors to the movie
  List<Actor__c> relatedActors = [
    SELECT id, Gender__c
    FROM Actor__c
    WHERE id IN (SELECT Actor__c FROM MovieActor__c WHERE Movie__c IN :movieId)
  ];

  // calculate male and female actos percent
  Integer total = relatedActors.size();
  Decimal femalActors = 0;
  Decimal maleActors = 0;

  for (Actor__c actor : relatedActors) {
    if ((actor.Gender__c).equalsIgnoreCase('Male')) {
      maleActors++;
    } else {
      femalActors++;
    }
  }

  for (Movie__c movie : movieToUpdate) {
    movie.FemaleActorsPercentage__c = (femalActors * 100) / total;
    movie.MaleActorsPercentage__c = (maleActors * 100) / total;
  }
  update movieToUpdate;

}
